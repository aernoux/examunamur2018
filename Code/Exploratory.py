"""PART 1: DETERMINATION OF PROBABILITIES"""

import pandas as pd

path = "../Data/"

Coffeebar = pd.read_csv(path+"Coffeebar_2013-2017.csv", sep=";")

Coffeebar = Coffeebar.fillna("Nothing")

#What food and drinks are sold by the coffeebar?

print("Food sold by the CoffeeBar : ")
for type_food in Coffeebar['FOOD'].unique():
    if type_food != "Nothing":
        print("- " + str(type_food))

print("Drinks sold by the CoffeeBar : ")
for type_drink in Coffeebar['DRINKS'].unique():
    print("- " + str(type_drink))

# How many unique customers did the bar have?

print("Number of unique customer : " + str(len(Coffeebar['CUSTOMER'].unique())))

#Make at least these plots: sold food and sold drinks

Nfood = Coffeebar['FOOD'].value_counts()
Nfood = Nfood.drop(Nfood.index[[0]])

Nfood.plot(kind="bar", title='Total amount of sold food over the five years')

Ndrinks = Coffeebar['DRINKS'].value_counts()
Ndrinks.plot(kind="bar", title='Total amount of sold drinks over the five years')

#Determine the average that customer buys a certain food or drink at any given time
Coffeebar['time'] = Coffeebar['TIME'].str[-8:]

probabilityFood = pd.DataFrame()
probabilityFood['Hour'] = Coffeebar['time'].unique()
probabilityFood = probabilityFood.set_index('Hour')
probabilityFood['Total orders'] = pd.Series(Coffeebar.groupby(['time'])['time'].count(), index=probabilityFood.index)
for type_food in Coffeebar['FOOD'].unique():
    probabilityFood[type_food] = pd.Series(round(Coffeebar.loc[Coffeebar['FOOD'] == type_food].groupby(['time'])['FOOD'].count() * 100
                                  / probabilityFood['Total orders'], 3), index=probabilityFood.index)
    probabilityFood = probabilityFood.fillna(0)
probabilityFood.to_csv(path + "Stats-Food-Coffeebar_2013-2017.csv", sep=";", encoding='utf-8')

probabilityDrink = pd.DataFrame()
probabilityDrink['Hour'] = Coffeebar['time'].unique()
probabilityDrink = probabilityDrink.set_index('Hour')
probabilityDrink['Total orders'] = pd.Series(Coffeebar.groupby(['time'])['time'].count(), index=probabilityDrink.index)
for type_drink in Coffeebar['DRINKS'].unique():
    probabilityDrink[type_drink] = pd.Series(round(Coffeebar.loc[Coffeebar['DRINKS'] == type_drink].groupby(['time'])['DRINKS'].count() * 100
                                  / probabilityDrink['Total orders'], 3), index=probabilityDrink.index)
probabilityDrink.to_csv(path + "Stats-Drinks-Coffeebar_2013-2017.csv", sep=";", encoding='utf-8')


# Give an example of Data Science application for this Coffeebar (Max 10 lignes)
##See the Paper