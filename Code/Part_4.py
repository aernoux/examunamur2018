"""PART 4: ADDITIONNAL QUESTIONS"""

#IMPORTATION OF SIMULATION IN PART 3
from random import randint
import pandas as pd


class Customer(object):
    def __init__(self, customerID, budget):
        self.customerID = customerID
        self.budget = budget

    def buyFood(self, priceFood, probabilityFood, hour):  # Lors de la simulation, quand je vais remplacer par Food, toutes probabilités seront transformés en Food
        decision = randint(0, 100)
        listFood = list(probabilityFood.columns)
        del listFood[0]
        proba = 0
        for type_Food in listFood:
            proba += probabilityFood[type_Food][hour]
            if decision <= proba:  # probabilités cumulées de consommer une nourriture
                Food = type_Food
                if not (type_Food == 'Nothing'):
                    self.budget -= priceFood[Food]
                return Food

    def buyDrink(self, priceDrink, probabilityDrink, hour):
        decision = randint(0, 100)
        listDrink = list(probabilityDrink.columns)
        del listDrink[0]
        proba = 0
        for type_Drink in listDrink:
            proba += probabilityDrink[type_Drink][hour]
            if decision <= proba:  # probabilités cumulées de consommer une nourriture
                Drink = type_Drink
                self.budget -= priceDrink[Drink]
                return Drink


class Onetime(Customer):
    def __init__(self, *args, **kwargs):
        Customer.__init__(self, *args, **kwargs)


class TripadvisorCust(Onetime):
    def __init__(self, tip, *args, **kwargs, ):
        Onetime.__init__(self, tip, *args, **kwargs)

    def tip(self):
        Tip = randint(1, 10)  # openclassroom
        self.budget -= Tip
        self.tip = Tip

class Manytimes(Customer):
    def __init__(self, *args, **kwargs):
        Customer.__init__(self, *args, **kwargs)

    def history(self, newDF):
        print('\nThere is the history of %s:' % (self.customerID))
        print(newDF[newDF['customerID'] == self.customerID][['time','drink','food']])


class HipsterCust(Manytimes):
    def __init__(self, *args, **kwargs):
        Manytimes.__init__(self, *args, **kwargs)


# PART 3: SIMULATION is needed to compute the changes of part 4

path = "../Data/"

Coffeebar = pd.read_csv(path + "Coffeebar_2013-2017.csv", sep=";")
probabilityDrink = pd.read_csv(path + "Stats-Drinks-Coffeebar_2013-2017.csv", sep=";").set_index('Hour')
probabilityFood = pd.read_csv(path + "Stats-Food-Coffeebar_2013-2017.csv", sep=";").set_index('Hour')

priceDrink = {"frappucino": 4, "soda": 3, "coffee": 3, "tea": 3, "water": 2, "milkshake": 5}
priceFood = {"sandwich": 5, "pie": 3, "cookie": 2, "muffin": 3}

newDF = pd.DataFrame(columns=['time', 'customerID', 'drink', 'food'])
newDF['time'] = Coffeebar['TIME']
for type_drink in priceDrink.values():
    CostDrink = type_drink
for type_food in priceFood.values():
    CostFood = type_food
BudgetMinimum = CostDrink + CostFood
alltime = list(Coffeebar['TIME'])

Returning = []
customerID = 80000000

for i in range(1000):
    choice = randint(1, 3)

    if choice == 1:
        c = HipsterCust(("CID") + str(customerID), 500)

    else:
        c = Manytimes(("CID" + str(customerID)), 250)
    customerID += 1
    Returning.append(c)

# customerID = 80001000
counter_index = 0
NoMoneyReturning = []
Returning_flag = False
for time in alltime:
    hour = time[-8:]

    ProbaCust = randint(1, 100)
    if ProbaCust in range(1, 20):
        # returningCust
        index_ReturningC = randint(0, len(Returning) - 1)
        current_customer = Returning[index_ReturningC]
        Returning_flag = True

    else:  # elif ProbaCust in range (21,100):
        # Pas returning
        if ProbaCust in range(21, 28):
            # Tripadvisor
            current_customer = TripadvisorCust('CID' + str(customerID), 100)
            current_customer.tip()
        else:  # ProbaCust in range (30,100)
            # Onetime
            current_customer = Onetime('CID' + str(customerID), 100)
    customerID += 1
    newDF['food'][counter_index] = current_customer.buyFood(priceFood, probabilityFood, hour)
    newDF['drink'][counter_index] = current_customer.buyDrink(priceDrink, probabilityDrink, hour)
    newDF['customerID'][counter_index] = current_customer.customerID
    counter_index += 1

    print(str(customerID+1))

    if Returning_flag and current_customer.budget < BudgetMinimum:
        Returning.remove(current_customer)
        NoMoneyReturning.append(current_customer)

newDF.to_csv(path + "newDF_2013-2017.csv", sep=";", encoding='utf-8')
newDF = pd.read_csv(path + "newDF_2013-2017.csv", sep=";")

# Part 4 bullet 1
choice = randint(0, len(Returning) - 1)
Returning[choice].history(newDF)
NoMoneyReturning[choice].history(newDF)

#Part 4 bullet 2
"""path = "../Data/"
Coffeebar = pd.read_csv(path + "Coffeebar_2013-2017.csv", sep=";")
C = len(Coffeebar['CUSTOMER'])-len(Coffeebar['CUSTOMER'].value_counts())
print("There are " + str(C) + " returning customers.")

Coffeebar['time'] = Coffeebar['TIME'].str[-8:]
ProbaReturning_Onetime = pd.DataFrame()
ProbaReturning_Onetime['Hour'] = Coffeebar['time'].unique()
ProbaReturning_Onetime = ProbaReturning_Onetime.set_index('Hour')
ProbaReturning_Onetime['Total orders'] = pd.Series(Coffeebar.groupby(['time'])['time'].count(), index=ProbaReturning_Onetime.index)
for item in Coffeebar['CUSTOMER'].unique():
    ProbaReturning_Onetime[item] = pd.Series(round(Coffeebar.loc[Coffeebar['CUSTOMER'] == item].groupby(['time'])['CUSTOMER'].count() * 100
                                  / ProbaReturning_Onetime['Total orders'], 3), index=ProbaReturning_Onetime.index)

ProbaReturning_Onetime.to_csv(path + "ProbaReturning_Onetime.csv", sep=";", encoding='utf-8')"""""

#Part 4 bullet 3
#replace 1000 by 50 at line 98

#Part 4 bullet 4

"""Coffeebar['time'] = Coffeebar['TIME'].str[-8:]
probabilityFood = pd.DataFrame()
probabilityFood['Hour'] = Coffeebar['time'].unique()
probabilityFood['date'] = Coffeebar['TIME'].str[0:5]
probabilityFood = probabilityFood.set_index('Hour')
probabilityFood['Total orders'] = pd.Series(Coffeebar.groupby(['time'])['time'].count(), index=probabilityFood.index)
for type_food in Coffeebar['FOOD'].unique():
    probabilityFood[type_food] = pd.Series(round(Coffeebar.loc[Coffeebar['FOOD'] == type_food].groupby(['time'])['FOOD'].count() * 100
                                  / probabilityFood['Total orders'], 3), index=probabilityFood.index)
    probabilityFood = probabilityFood.fillna(0)
probabilityFood.to_csv(path + "Stats-Food-Coffeebar_2013-2017.csv", sep=";", encoding='utf-8')

probabilityDrink = pd.DataFrame()
probabilityDrink['Hour'] = Coffeebar['time'].unique()
probabilityDrink['date'] = Coffeebar['TIME'].str[0:5]
probabilityDrink = probabilityDrink.set_index('Hour')
probabilityDrink['Total orders'] = pd.Series(Coffeebar.groupby(['time'])['time'].count(), index=probabilityDrink.index)
for type_drink in Coffeebar['DRINKS'].unique():
    probabilityDrink[type_drink] = pd.Series(round(Coffeebar.loc[Coffeebar['DRINKS'] == type_drink].groupby(['time'])['DRINKS'].count() * 100
                                  / probabilityDrink['Total orders'], 3), index=probabilityDrink.index)
probabilityDrink.to_csv(path + "Stats-Drinks-Coffeebar_2013-2017.csv", sep=";", encoding='utf-8')


def buyFood(self, priceFood, probabilityFood, hour,
            date):  # Lors de la simulation, quand je vais remplacer par Food, toutes probabilités seront transformés en Food
    decision = randint(0, 100)
    listFood = list(probabilityFood.columns)
    del listFood[0]
    proba = 0
    for type_Food in listFood:
        proba += probabilityFood[type_Food][hour]
        if date < str(2015):
            if decision <= proba:  # probabilités cumulées de consommer une nourriture
                Food = type_Food
                if not (type_Food == 'Nothing'):
                    self.budget -= priceFood[Food]
        else:
            if decision <= proba:  # probabilités cumulées de consommer une nourriture
                Food = type_Food
                if not (type_Food == 'Nothing'):
                    self.budget -= priceFood[Food] * 1.2
                return Food


def buyDrink(self, priceDrink, probabilityDrink, hour, date):
    decision = randint(0, 100)
    listDrink = list(probabilityDrink.columns)
    del listDrink[0]
    proba = 0
    for type_Drink in listDrink:
        proba += probabilityDrink[type_Drink][hour][date]
        if date < str(2015):
            if decision <= proba:  # probabilités cumulées de consommer une nourriture
                Drink = type_Drink
                self.budget -= priceDrink[Drink]
                return Drink
        else:
            if decision <= proba:  # probabilités cumulées de consommer une nourriture
                Drink = type_Drink
                self.budget -= priceDrink[Drink] * 1.2
                return Drink
add the line 127 : date = Coffeebar['TIME'].str[0:5]"""""

#Part 4 bullet 5
#replace 500 by 40 at line 102

#Part 4 bullet 6
#line 117: 20 becomes 10. In the line 125 range(21, 28) becomes range(11, 40).