# DATA ANALYTICS: ExamUNamur2018 - Coffee bar simulation

##Description
The project consist in investigating and making simulations on five years of data in order to answer several questions.
For example, determine the probability that a customer buys a certain food or drink at any given time.
We will use Python 3 (Anaconda) as interpreter and Pycharm as one of the most popular IDE’s for Python.
Moreover, we use Git and Bitbucket as version control system.

## Structure

The course “Data Analytics” is given by Gert De Geyter at the University of Namur.
For this course, we have to analyze and implement a code from a data set with five years of data, in order to analyze
data from a Coffee bar.
In few words, the Coffee bar has customers who come more than once called “returning time customers”
– some of them are called “hipsters customers”, others who come only once are called “one-time customers”
– some of them are called “tripadvisor customers”. This project consists in simulating five years span.

The first part is an investigation of the data set to answer some questions related to the Coffee bar.
In the second part, the insight collected on the data is used to create more realistic customers thanks to classes and its hierarchy.
Then, the third part is about a simulation by manipulating our code and implementing real facts the coffee could face.
The final part involves advanced questions in which the impact is measured and asks reflection and interpretation about
what we understood up to this part.
This paper is individual and the explanations and interpretations I give here will tell you how I analyzed and resolved this project.


### Prerequisites

What things you need to install the software and how to install them:

```
Git
Bitbucket account
Anaconda 3 (Python 3.x)
Pycharm
```

### Installing

Steps:

```
Installing git (WINDOWS):

Go to https://git-scm.com/
Click download …. for windows
After downloading open the file
On the license page click “Next”
On the next page make sure “Git Bash here” is checked before clicking “Next”
On the next screen the default should be fine.
Make sure “Use Git from the windows command prompt” is check
Press “Next” on all following screens
Finally click “Install” to start the installation
On the final screen make sure to click “Launch git bash” before clicking finish
(in case you forgot search it in the windows menu)
We will now have the one time git setup.
You only have to run this the first time you run git on your laptop
Open Git Bash (search in Windows)
Set your username (name following is an example)
git config -- global user.name "Jack Smith“
Set your email
git config --global user.email jacksmith@example.com

+ Bitbucket account

```

```
Installing Anacond installer with Python 3.6:

Go to https://www.anaconda.com/download/
Click the 3.6 version
It is better to click the 64-bit installer
Next screens click “Next”
Next screen keep a close attention to the destination folder!
This is important in the case PyCharm does not find the correct python version
Luckily, the next screen has the option to register Anaconda as the default python 3.6
Click "Install"

```

```

Installing Pycharm:

Go to https://www.jetbrains.com/pycharm/download
Download the Community Edition
On the next screens keep on pressing “Next”.
You can check the “create .py associations” if you want.

```

## Running the tests

Through the python console:
interactive, interpreted line by line, not stored

Running an entire script:
compiled and interpreted at runtime, stored

Through an iPython Notebook:
interpreted line by line, stored, less clear what happens(will be handled in later courses)


### Break down into end to end tests

```
Using Git on Pycharm and Bitbucket we can:

Create and verify new accounts.
Import a list of contacts, in the form of email addresses.

Some of these are existing members of the network site (set up in the test fixture), and accept the new user into their network.
Some of the contacts are not existing members, triggering some kind of invitation flow.
```

### And coding style tests

```
PEP:	8
Title:	Style Guide for Python Code
Author:	Guido van Rossum <guido at python.org>, Barry Warsaw <barry at python.org>, Nick Coghlan <ncoghlan at gmail.com>
Status:	Active
Type:	Process
Created:	05-Jul-2001
Post-History:	05-Jul-2001, 01-Aug-2013
```



## Acknowledgments

* Gert De Geyter
* Christian Colot
* Students colleagues of IM Engineering
